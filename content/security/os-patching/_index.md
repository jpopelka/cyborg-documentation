---
title: Running supported and updated systems
linkTitle: OS Patching
description: >
  Monitoring and processes to keep systems updated and supported
---

In general, systems should be

1. running a supported operating system
1. kept up-to-date with security updates

Depending on the operating system, these aspects can be more or less automated.

See below for a list of examples how these aspects are managed by different
teams.

## Running a supported operating system

Depending on the operating system, the amount of manual work needed to keep
systems supported, i.e. able to receive updates, can vary.

For operating systems with completely rolling releases like openSUSE Tumbleweed
or Debian Testing, no manual intervention is needed. For operating systems with
partially rolling releases like CentOS Stream, manual intervention is only
needed every couple of years for major version upgrades. For operating systems
with regular releases like Ubuntu, RHEL or Fedora, manual intervention is
needed in an interval roughly equivalent to the support period per release.

For operating systems with non-rolling releases, monitoring of the deployed
versions is required to ensure that no unsupported operation system versions
are in use.

## Keeping systems up-to-date with (security) updates

Independent of the release cadence, systems need to be kept up-to-date with at
least security updates. Depending on the updates and operating system, systems
might need to be rebooted for the updates to take effect, e.g. for kernel
updates.
