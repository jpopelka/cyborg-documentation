---
title: 'CKI: automatically applying updates via dnf-automatic'
linkTitle: 'CKI: dnf-automatic'
description: >
  How the CKI project automatically manages updates and subsequent reboots on VMs
  and bare-metal machines via dnf-automatic
---

## Problem

For image-based deployments via container and VM images, keeping systems up to
date is mostly solved by periodically recreating these machines.

For bare-metal machines or environments that don't support image-based
deployments, (security) updates need to be periodically installed and applied.

The following steps document a way to do this via dnf-automatic for the case
where redundant machines are available for a certain task, i.e. where machines
are allowed to be offline on an individual basis.

The relevant Ansible role of the CKI project can be found in
[com/infrastructure].

## General idea

Machines are going to be updated once a week via [dnf-automatic], including
automatic reboot on kernel updates. Updates are deterministically applied on
different weekdays for different machines of a group. Monitoring is used to
alert when a machine is unhealthy after updates are applied.

{{% alert title="Warning" color="warning" %}}
Updates are applied via [dnf-automatic] in the background. This can cause
trouble if those updates affect running services. A recent example is an
updated containerd package that broke running moby containers. For that
reason, the CKI project now uses [offline-upgrade](cki-offline-upgrade.md) for
updating.
{{% /alert %}}

## Steps

1. For each machine, an update weekday needs to be determined. This can either
   be done automatically based on some characteristic like network segment or
   availability zone, or manually via the Ansible inventory:

   - Automatically based on AZ:

     ```yaml
     # vars.yml
     dnf_automatic_az_weekday_mappings:
       a: Tue
       b: Wed
       c: Thu
       d: Fri
       default: Tue
     ```

     ```yaml
     # playbook.yml for an EC2 machine
     - ansible.builtin.set_fact:
         dnf_automatic_weekday: "{{
             dnf_automatic_az_weekday_mappings
             [placement.availability_zone | last] |
             default(dnf_automatic_az_weekday_mappings.default)
           }}"
     ```

   - Manually in the inventory:

     ```yaml
     # inventory.yml
     all:
       hosts:
         first-machine:
           dnf_automatic_weekday: Tue
         second-machine:
           dnf_automatic_weekday: Wed
     ```

   In general, Tuesday, Wednesday or Thursday are the best days for machines
   where a reboot is fast. For machines where draining might take longer than a
   day, days need to be selected that are far enough apart to ensure that there
   will always be only one machine going through an update at any time.

1. While [dnf-automatic] comes with its own timers, none provides the
   functionality required here. The following template and tasks configure
   updates once a week on the given weekday:

   ```toml
   # templates/dnf-automatic-install-weekly.timer
   [Unit]
   Description=dnf-automatic-install-weekly timer
   ConditionPathExists=!/run/ostree-booted
   Wants=network-online.target

   [Timer]
   # change time zone so enough people are online to see stuff break
   OnCalendar={{ dnf_automatic_weekday }} *-*-* 10:00 UTC
   RandomizedDelaySec=60m
   Persistent=true
   Unit=dnf-automatic-install.service

   [Install]
   WantedBy=timers.target
   ```

   ```yaml
   # playbook.yml
   - ansible.builtin.package:
       name: dnf-automatic
       state: present

   - ansible.builtin.template:
       src: dnf-automatic-install-weekly.timer
       dest: /etc/systemd/system/dnf-automatic-install-weekly.timer
       mode: '0644'

   - ansible.builtin.systemd:
       name: dnf-automatic-install-weekly.timer
       enabled: true
       state: started
       daemon_reload: true
   ```

1. While updates to services might be automatically applied, this is not the
   case for e.g. kernel updates. In that case, an updated kernel requires a
   machine reboot to take any effect.

   The following example script implements this. It first checks whether a
   reboot is really needed by comparing the last available kernel with the
   kernel currently running. If the running kernel is outdated, it will
   manually stop certain systemd services that should be stopped *before* the
   machine is rebooted. This is repeated in a loop as `systemctl stop` might
   fail if another `systemctl start` invocation happens in the background.
   An example of a service where this is needed is [gitlab-runner], which can
   be configured to wait a long time until all currently running jobs are
   finished. The script will terminate with a non-zero exit code in the case
   that a reboot is needed.

   ```bash
   #!/usr/bin/bash
   # /usr/local/sbin/reboot-if-needed-check.sh

   set -euo pipefail

   if rpm -q kernel-core | tail -n 1 | grep -F "$(uname -r)"; then
       exit 0
   fi

   IFS=' ' read -ra services <<< "${SERVICES_TO_STOP:-}"
   for service in "${services[@]}"; do
       while ! systemctl stop "${service}"; do
           echo "Failed to stop ${service}, retrying"
       done
   done

   exit 1
   ```

   This script can be hooked into systemd to run after each dnf-automatic
   systemd unit execution via the following templates and tasks:

   ```toml
   # templates/reboot-if-needed.service
   [Unit]
   Description=automatically reboot machine if needed
   FailureAction=reboot

   [Service]
   Type=oneshot
   Environment="SERVICES_TO_STOP={{ services_to_stop }}"
   ExecStart=/usr/local/sbin/reboot-if-needed-check.sh
   ```

   ```toml
   # templates/trigger-reboot-if-needed.conf
   [Unit]
   OnSuccess=reboot-if-needed.service
   OnFailure=reboot-if-needed.service
   ```

   ```yaml
   # playbook.yml
   - ansible.builtin.template:
       src: reboot-if-needed-check.sh
       dest: /usr/local/sbin/reboot-if-needed-check.sh
       mode: '0755'

   - ansible.builtin.template:
       src: reboot-if-needed.service
       dest: /etc/systemd/system/reboot-if-needed.service
       mode: '0644'

   - ansible.builtin.file:
       path: /etc/systemd/system/dnf-automatic-install.service.d
       state: directory
       mode: '0755'

   - ansible.builtin.template:
       src: trigger-reboot-if-needed.conf
       dest: /etc/systemd/system/dnf-automatic-install.service.d/trigger-reboot-if-needed.conf
       mode: '0644'
   ```

1. Finally, proper monitoring is required so that unhealthy machines can be
   detected and fixed promptly. This is left as an exercise for the reader 🤗.

[com/infrastructure]: https://gitlab.com/cki-project/infrastructure/-/tree/main/ansible/roles/dnf_automatic/
[dnf-automatic]: https://dnf.readthedocs.io/en/latest/automatic.html
[gitlab-runner]: https://docs.gitlab.com/runner/
