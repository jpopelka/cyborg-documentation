---
title: General recommendations
linkTitle: General
description: >
  General recommendations for deployments to a Kubernetes cluster.
weight: -10
---

## Use templating for your jobs

See [CKI example].

## Monitor events in your namespace

You can do that via [`caicloud/event_exporter`] or [`cki-project/cki-tools - k8s_event_listener`].

[CKI example]: https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/openshift/templates/40-cronjob.yml.j2
[`caicloud/event_exporter`]: https://github.com/caicloud/event_exporter
[`cki-project/cki-tools - k8s_event_listener`]: https://gitlab.com/cki-project/cki-tools/-/blob/main/documentation/cki.cki_tools.k8s_event_listener.md
